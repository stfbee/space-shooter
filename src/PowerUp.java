/**
 * User: vladik
 * Date: 26.09.13
 * Time: 13:03
 */
@SuppressWarnings({"WeakerAccess", "UnusedDeclaration"})
public class PowerUp extends Enemy {
    public final int TYPE;

    public PowerUp(int type) {

        TYPE = type;

    }
}
