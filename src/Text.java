/**
 * User: vladik
 * Date: 22.09.13
 * Time: 15:00
 */
class Text {
    public final int x;
    public final int y;
    public final String string;
    public double alpha= 255;
    public final int maxDistance = 25;
    public int distance = 0;

    @SuppressWarnings("SameParameterValue")
    public Text(int x1, int y1, String string) {
        this.x = x1;
        this.y = y1;
        this.string = string;
    }

    @SuppressWarnings("UnusedParameters")
    public void update(double delta){
        alpha -= 255 / maxDistance;
        distance += 1;
    }
}
