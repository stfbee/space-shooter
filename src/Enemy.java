import java.util.Random;

/**
 * User: vladik
 * Date: 19.09.13
 * Time: 14:34
 */
class Enemy {
    public int x1, y1;
    private double velocity;
    private final int angle;
    private double position;
    public boolean busted = false;
    private final Random random = new Random();

    public Enemy() {
        angle = random.nextInt(360);
        position = (int) Math.sqrt((Game.HEIGHT * Game.HEIGHT / 4) + (Game.WIDTH * Game.WIDTH / 4));
    }

    public void move(double delta) {
        double SPEED = .001;
        velocity += SPEED * delta;
        position -= velocity * delta;
        x1 = (int) (Math.cos(angle * Math.PI / 180) * position);
        y1 = (int) (Math.sin(angle * Math.PI / 180) * position);
        checkHit();
    }

    private void checkHit() {

        /**
         * вычисление попадания
         */

        if (!busted && Game.bullets.size() != 0) {
            int bulletsSize = Game.bullets.size();
            for (int k = 0; k < bulletsSize; k++) {

                if (Game.bullets.size() <= k) {
                    break;
                }

                Bullet b = Game.bullets.get(k);
                int bulletX = b.x2;
                int bulletY = b.y2;

                int distance = (int) Math.sqrt((x1 - bulletX) * (x1 - bulletX) + (y1 - bulletY) * (y1 - bulletY));
                if (distance <= 6) {
                    Game.bullets.remove(k);
                    busted = true;
                    Game.combo++;
                    Game.kills++;
                    Game.bulletKills++;

                    Game.playSound("hit" + (random.nextInt(3) + 1));
                    if (Game.bulletKills % 10 == 0) {
                        Game.flashes++;
                        Game.playSound("powerup" + (random.nextInt(2) + 1));
                        Game.texts.add(new Text(bulletX, bulletY, "SHIELD +1!"));
                    }
                    break;
                }
            }
        }

        if (!busted && Game.stars.size() != 0) {
            int starsSize = Game.stars.size();
            for (int k = 0; k < starsSize; k++) {

                if (Game.stars.size() <= k) {
                    break;
                }
                Star s = Game.stars.get(k);
                int bulletX = s.x2;
                int bulletY = s.y2;

                int distance = (int) Math.sqrt((x1 - bulletX) * (x1 - bulletX) + (y1 - bulletY) * (y1 - bulletY));
                if (distance <= 6) {
                    Game.stars.remove(k);
                    busted = true;
                    Game.playSound("hit" + (random.nextInt(3) + 1));
                    Game.kills++;
                    break;
                }
            }
        }

        int x = x1 + Game.centerX;
        int y = -y1 + Game.centerY;

        if (!busted){
            int distance = (int) Math.sqrt((x - Game.centerX) * (x - Game.centerX) + (y - Game.centerY) * (y - Game.centerY));
            if (distance == 5 || distance == 4) {
                Game.playSound("explosion" + (random.nextInt(3) + 1));
                Game.gameOver = true;
            }
        }

        if (busted){
            Game.enemies.remove(this);
        }
    }
}
