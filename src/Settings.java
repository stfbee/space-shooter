import java.io.*;
import java.util.HashMap;
import java.util.Properties;

/**
 * User: vladik
 * Date: 11.10.13
 * Time: 10:07
 */
public class Settings {
    HashMap<String, String> settings = new HashMap<String, String>();
    StringBuilder sb = new StringBuilder();
    static Properties p = new Properties();
    static int highscore;


    public Settings() {

        for (int i = 0; i < settings.size(); i++){
            sb.append("highscore:").append(Integer.toString(0));
        }

        loadSettings();
    }

    public void loadSettings() {
        String path = System.getProperty("user.home") + File.separator + ".spaceshooter" + File.separator;
        String file = "spaceshooter.properties";
        File f = new File(path + file);
        if (f.exists()) {

            try {
//                BufferedReader input = new BufferedReader(new FileReader(f));
//                String line[] = input.readLine().split(": ");
//                if (line[0].equals("highscore"))
                BufferedInputStream stream = new BufferedInputStream(new FileInputStream(path + file));
                p.load(stream);
                stream.close();
                highscore = Integer.parseInt(p.getProperty("highscore"));

            } catch (FileNotFoundException ignored) {
            } catch (IOException ignored) {
            } catch (NumberFormatException ignored) {
                highscore = 0;
            }
        } else {
            highscore = 0;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void saveSettings() {
        String path = System.getProperty("user.home") + File.separator + ".spaceshooter" + File.separator;
        String file = "highscore.txt";
        File f = new File(path);
        f.mkdir();
        try {
            f = new File(path + file);
            BufferedWriter output = new BufferedWriter(new FileWriter(f));
            output.write(String.valueOf(highscore));
            output.close();
        } catch (IOException ignored) {
        }
    }
}