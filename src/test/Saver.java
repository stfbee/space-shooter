package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * User: vladik
 * Date: 04.10.13
 * Time: 13:29
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public class Saver {
    public static void main(String[] args) {
        String path = System.getProperty("user.home") + File.separator + ".spaceshooter" + File.separator;
        String file = "highscore.txt";
        File f = new File(path);
        f.mkdir();
        try {
            f = new File(path+file);
            BufferedWriter output = new BufferedWriter(new FileWriter(f));
            output.write("314235");
            output.close();
        } catch (IOException ignored) {}
    }
}
