import java.awt.*;
import java.util.Random;

/**
 * User: vladik
 * Date: 20.09.13
 * Time: 10:10
 */
class Star {
    public int x1, x2, y1, y2;
    private final int angle;
    public Color color;
    private double velocity;
    private double position;

    public Star() {
        Random rand = new Random();
        angle = rand.nextInt(360);

        int r = 255 - rand.nextInt(30);
        int g = 255 - rand.nextInt(30);
        int b = 255 - rand.nextInt(30);
        int a = 255;

        if (r > b) {
            b = 0;
        } else {
            r = 0;
        }

        color = new Color(r, g, b, a);
        color = Color.WHITE;
    }

    public void move(double delta) {
        double SPEED = .225;
        velocity += SPEED * delta;
        position += velocity * delta;
            x1 = (int) (Math.cos(angle * Math.PI / 180) * position);
            y1 = (int) (Math.sin(angle * Math.PI / 180) * position);
            x2 = (int) (Math.cos(angle * Math.PI / 180) * (position + 5));
            y2 = (int) (Math.sin(angle * Math.PI / 180) * (position + 5));

    }
}
