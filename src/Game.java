import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * User: Vlad
 * Date: 18.09.13
 * Time: 19:04
 * Space Shooter - basic retro 2d space shooter game
 * Copyright (C) 2013  Vladislav Onischenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

@SuppressWarnings("ConstantConditions")
public class Game extends JPanel {
    //Размеры поля
    public static final int HEIGHT = 720; //480;
    public static final int WIDTH = 1024; //640;
    public static final List<Bullet> bullets = new ArrayList<Bullet>();
    public static final List<Enemy> enemies = new ArrayList<Enemy>();
    public static final List<Star> stars = new ArrayList<Star>();
    public static final List<Text> texts = new ArrayList<Text>();
    private static final double[][] gunPoints = new double[3][2];
    private static final int radiusSquared = HEIGHT * HEIGHT / 4 + WIDTH * WIDTH / 4;
    private static final int gunSize = 20;
    private static final int gunSpeed = 100;
    private static final Random random = new Random();
    public static int combo;
    public static int kills;
    public static int bulletKills;
    public static int flashes;
    public static int centerX;
    public static int centerY;
    public static boolean gameOver = false;
    private static int highscore;
    //    public static boolean powerUp_shield = false;
//    public static boolean powerUp_laser = false;
//    public static boolean powerUp_double = false;
//    public static boolean powerUp_accuracy = false;
//    public static int powerUp_shield_type = 0;
//    public static int powerUp_laser_type = 1;
//    public static int powerUp_double_type = 2;
//    public static int powerUp_accuracy_type = 3;
    private static int shots;
    private static int fps;
    private static int currentFPS;
    private static int mouseX;
    private static int mouseY;
    private static BufferedImage starsImage;
    private static BufferedImage canvas;
    private static boolean debug = false;
    private static boolean gameRunning = false;
    private static boolean shooting = false;
    private static long lastFpsTime;
    private static long lastGunTime;
    private static double angle;
    private static JFrame f;
    private static boolean paused;
    //    private final HashMap<String, String> settings = new HashMap<String, String>();
    private static Settings settings = new Settings();
    private final Font boldFont = new Font("", Font.BOLD, 15);
    private final Font plainFont = new Font("", Font.PLAIN, 15);

    public Game() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
    }

    private static void restart() {
        kills = 0;
        shots = 0;
        combo = 0;
        bulletKills = 0;
        flashes = 5;
        gameOver = false;
        bullets.clear();
        stars.clear();
        enemies.clear();
        texts.clear();
    }

    public static void main(String[] args) {
        f = new JFrame("Space Shooter - v0.9");
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Game game = new Game();
        f.add(game);
        f.setResizable(false);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);

        centerX = (f.getWidth() - (f.getInsets().left + f.getInsets().right)) / 2;
        centerY = (f.getHeight() - (f.getInsets().top + f.getInsets().bottom)) / 2 + 1;

        f.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                if (!gameOver) rotate(e.getX(), e.getY());
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                long time = System.currentTimeMillis();
                if (!gameOver) {
                    rotate(e.getX(), e.getY());
                    if (time - lastGunTime >= gunSpeed) {
                        shot();
                        lastGunTime = time;
                    }
                }
            }
        });

        f.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (!gameOver && !paused) {
                    shot();
                    shooting = true;
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (!gameOver && !paused) {
                    shooting = false;
                }
            }
        });

        f.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (!gameOver) {
                    if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                        spawnStars();
                    } else if (e.getKeyCode() == KeyEvent.VK_D) {
                        debug = !debug;
                    } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        paused = !paused;
                    }
                } else {
                    restart();
                }
            }
        });

        starsImage = generateStars();
        gameRunning = true;
        restart();
        Joystick();
//        loadHS();
        game.gameLoop();
    }

    private static void spawnStars() {
        if (flashes > 0) {
            for (int i = 1; i < 101; i++) {
                stars.add(new Star());
            }

            playSound("flash" + (random.nextInt(2) + 1));
            flashes--;
        }
    }

    private static void shot() {
        playSound("shot" + (random.nextInt(5) + 1));
        bullets.add(new Bullet((int) angle));
        shots++;
    }

    private static void spawnEnemy() {
        enemies.add(new Enemy());
    }

    private static void rotate(int x, int y) {
        mouseX = x - centerX - f.getInsets().left;
        mouseY = -(y - centerY - f.getInsets().top);
        angle = get_angle(mouseX, mouseY);
        changeAngles();
    }

    private static void changeAngles() {
        gunPoints[0][0] = Math.cos(angle * Math.PI / 180) * gunSize;
        gunPoints[0][1] = Math.sin(angle * Math.PI / 180) * gunSize;
        angle += 140;
        gunPoints[1][0] = Math.cos(angle * Math.PI / 180) * gunSize;
        gunPoints[1][1] = Math.sin(angle * Math.PI / 180) * gunSize;
        angle += 80;
        gunPoints[2][0] = Math.cos(angle * Math.PI / 180) * gunSize;
        gunPoints[2][1] = Math.sin(angle * Math.PI / 180) * gunSize;
        angle -= 220;
    }

    private static BufferedImage generateStars() {
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        int len = WIDTH * HEIGHT;
        int[] rgb = new int[len];
        image.getRGB(0, 0, WIDTH, HEIGHT, rgb, 0, WIDTH);
        for (int i = 0; i < 500; i++) {
            int index = random.nextInt(len);
            int a = random.nextInt(255);
            int r = 255 - random.nextInt(30);
            int g = 255 - random.nextInt(30);
            int b = 255 - random.nextInt(30);
            rgb[index] = (a << 24) | (r << 16) | (g << 8) | b;
        }
        image.setRGB(0, 0, WIDTH, HEIGHT, rgb, 0, WIDTH);
        return image;
    }

    private static double get_angle(float x, float y) {
        double a = Math.atan(y / x) * 180 / Math.PI;
        if (x < 0) a += 180;
        else if (y < 0 && x > 0) a += 360;
        else if (x == 0 && y < 0) a = 270;
        else if (y == 0 && x < 0) a = 180;
        else if (y == 0 && x == 0) a = 90;
        return a;
    }

    public static synchronized void playSound(final String file) {
        new Thread(new Runnable() {
            public void run() {
                URL yourFile = Game.class.getResource("/res/" + file + ".wav");
                AudioInputStream stream;
                AudioFormat format;
                DataLine.Info info;
                Clip clip;
                try {
                    stream = AudioSystem.getAudioInputStream(yourFile);
                    format = stream.getFormat();
                    info = new DataLine.Info(Clip.class, format);
                    clip = (Clip) AudioSystem.getLine(info);
                    clip.open(stream);
                    clip.start();
                    Thread.sleep(2000);
                    clip.stop();
                    clip.close();

                } catch (UnsupportedAudioFileException ignored) {
                } catch (IOException ignored) {
                } catch (LineUnavailableException ignored) {
                } catch (InterruptedException ignored) {
                } catch (IllegalStateException ignored) {
                }
            }
        }).start();
    }

    private static synchronized void Joystick() {
        new Thread(new Runnable() {
            public void run() {
                float x = 0, y = 0;
                long time = 0;
                while (true) {
                    Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
                    if (controllers.length == 0) {
                        System.out.println("Found no controllers.");
                        break;
                    }

                    for (Controller controller : controllers) {
                        controller.poll();
                        net.java.games.input.EventQueue queue = controller.getEventQueue();
                        Event event = new Event();
                        while (queue.getNextEvent(event)) {
                            Component comp = event.getComponent();
                            float value = event.getValue();
                            String name = comp.getName();

                            if (name.equals("y")) y = -value;
                            if (name.equals("x")) x = value;

                            if (name.equals("Start") && !gameOver && value == 1.0f) {
                                paused = !paused;
                            }

                            if (name.equals("X") && !gameOver && value == 1.0f) {
                                spawnStars();
                            }

                            if (name.equals("A")) {
                                if (value == 1.0f) {
                                    if (!gameOver) {
                                        shooting = true;
                                        time = 0;
                                    } else {
                                        restart();
                                    }
                                } else {
                                    shooting = false;
                                }
                            }

                            if (!paused && !gameOver) {
                                long now = System.nanoTime();
                                if (now - time > 100000000) {
                                    if (shooting) shot();
                                    time = now;
                                }
                            }

                            if (x * x + y * y > 0.35) {
                                angle = Game.get_angle(x, y);
                                changeAngles();
                            }

                        }
                    }
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException ignored) {
                    }
                }
            }
        }).start();
    }

    private static void update(double delta) {

        int enemiesSize = enemies.size();
        int starsSize = stars.size();
        int bulletsSize = bullets.size();
        int textsSize = texts.size();


        if (enemiesSize != 0) {
            for (int i = 0; i < enemiesSize; i++) {
                Enemy e = enemies.get(i);
                e.move(delta);
                if (e.busted) {
                    i = i - 1;
                    enemiesSize = enemies.size();
                }
            }
        }

        if (starsSize != 0) {
            for (int i = 0; i < starsSize; i++) {
                if (stars.size() <= i) {
                    break;
                }
                Star s = stars.get(i);

                s.move(delta);

                if (s.x1 * s.x1 + s.y1 * s.y1 > radiusSquared) {
                    stars.remove(i);
                    i = i - 1;
                    starsSize = stars.size();
                }
            }
        }

        if (bulletsSize != 0) {
            for (int i = 0; i < bulletsSize; i++) {
                if (bullets.size() <= i) {
                    break;
                }
                Bullet b = bullets.get(i);
                b.move(delta);
                if (b.x1 * b.x1 + b.y1 * b.y1 > radiusSquared) {
                    bullets.remove(i);
                    i = i - 1;
                    bulletsSize = bullets.size();
                    combo = 0;
                }
            }
        }

        if (textsSize != 0) {
            for (int i = 0; i < textsSize; i++) {
                Text t = texts.get(i);
                t.update(delta);
                if (t.distance == t.maxDistance) {
                    texts.remove(i);
                    i = i - 1;
                    textsSize = texts.size();
                }
            }
        }
    }

    private void render() {
        canvas = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = canvas.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.BLACK);
        g2.fillRect(0, 0, WIDTH, HEIGHT);
        g2.drawImage(starsImage, null, 0, 0);

        if (debug) {
            g2.setColor(Color.green);
            g2.drawLine(0, centerY, centerX * 2, centerY);
            g2.drawLine(centerX, 0, centerX, centerY * 2);
        }

        if (stars.size() != 0) {
            int starsSize = stars.size();
            for (int i = 0; i < starsSize; i++) {
                if (stars.size() <= i) {
                    break;
                }
                Star s = stars.get(i);
                int globalX1 = s.x1;
                int globalY1 = s.y1;
                int globalX2 = s.x2;
                int globalY2 = s.y2;
                int x1 = globalX1 + centerX;
                int y1 = -globalY1 + centerY;
                int x2 = globalX2 + centerX;
                int y2 = -globalY2 + centerY;
                g2.setColor(s.color);
                g2.drawLine(x1, y1, x2, y2);
            }
        }

        //Render gun
        g2.setColor(Color.BLACK);
        int[] x10 = new int[3];
        int[] y10 = new int[3];
        x10[0] = (int) (gunPoints[0][0] + centerX);
        x10[1] = (int) (gunPoints[1][0] + centerX);
        x10[2] = (int) (gunPoints[2][0] + centerX);

        y10[0] = (int) (-gunPoints[0][1] + centerY);
        y10[1] = (int) (-gunPoints[1][1] + centerY);
        y10[2] = (int) (-gunPoints[2][1] + centerY);
        int n = 3;

        g2.fillPolygon(x10, y10, n);     // Fills the triangle above.
        g2.setColor(Color.GREEN);
        g2.drawLine(centerX, centerY, (int) gunPoints[0][0] + centerX, (int) -gunPoints[0][1] + centerY);
        g2.drawLine(centerX, centerY, (int) gunPoints[1][0] + centerX, (int) -gunPoints[1][1] + centerY);
        g2.drawLine(centerX, centerY, (int) gunPoints[2][0] + centerX, (int) -gunPoints[2][1] + centerY);

        g2.drawLine((int) gunPoints[0][0] + centerX, (int) -gunPoints[0][1] + centerY, (int) gunPoints[1][0] + centerX, (int) -gunPoints[1][1] + centerY);
        g2.drawLine((int) gunPoints[1][0] + centerX, (int) -gunPoints[1][1] + centerY, (int) gunPoints[2][0] + centerX, (int) -gunPoints[2][1] + centerY);
        g2.drawLine((int) gunPoints[2][0] + centerX, (int) -gunPoints[2][1] + centerY, (int) gunPoints[0][0] + centerX, (int) -gunPoints[0][1] + centerY);


        if (bullets.size() != 0) {
            int bulletsSize = bullets.size();
            for (int i = 0; i < bulletsSize; i++) {
                if (bullets.size() <= i) {
                    break;
                }
                Bullet b = bullets.get(i);
                int globalX1 = b.x1;
                int globalY1 = b.y1;
                int globalX2 = b.x2;
                int globalY2 = b.y2;
                int x1 = globalX1 + centerX;
                int y1 = -globalY1 + centerY;
                int x2 = globalX2 + centerX;
                int y2 = -globalY2 + centerY;
                g2.setColor(b.color);
                g2.drawLine(x1, y1, x2, y2);
            }
        }

        if (enemies.size() != 0) {
            int enemiesSize = enemies.size();
            for (int i = 0; i < enemiesSize; i++) {
                if (enemies.size() <= i) {
                    break;
                }
                Enemy e = enemies.get(i);
                int globalX = e.x1;
                int globalY = e.y1;
                int x = globalX + centerX;
                int y = -globalY + centerY;
                g2.setColor(Color.RED);
                g2.fillOval(x - 6, y - 6, 12, 12);
            }
        }

        if (texts.size() != 0) {
            int textsSize = texts.size();
            for (int i = 0; i < textsSize; i++) {
                if (texts.size() <= i) {
                    break;
                }
                Text t = texts.get(i);

                int globalX1 = t.x;
                int globalY1 = t.y;
                int x = globalX1 + centerX;
                int y = -globalY1 + centerY;

                String text = t.string;
                g2.setFont(boldFont);
                g2.setColor(new Color(0, 255, 0, (int) t.alpha));
                FontMetrics fm = g2.getFontMetrics();
                Rectangle2D r = fm.getStringBounds(text, g2);


                g2.drawString(text, x - (int) (r.getWidth() / 2), y - t.distance);
            }
        }

        g2.setFont(plainFont);
        g2.setColor(Color.cyan);
        if (debug) {
            g2.drawString("SPACE SHOOTER - v0.9 - DEBUG MODE", 10, 30);
            g2.drawString("angle: " + (int) angle + "°", 10, 50);
            g2.drawString("mouseX: " + mouseX, 10, 70);
            g2.drawString("mouseY: " + mouseY, 10, 90);
            g2.drawString("shots: " + shots, 10, 110);
            g2.drawString("enemies: " + enemies.size(), 10, 130);
            g2.drawString("stars: " + stars.size(), 10, 150);
            g2.drawString("bullets: " + bullets.size(), 10, 170);
            g2.drawString("FPS: " + currentFPS, 10, 190);
        }
        if (gameOver) {
            if (kills > highscore) {
                highscore = kills;
//                saveHS();
                settings.saveSettings();
            }

            g2.setColor(Color.RED);
            g2.setFont(boldFont);

            FontMetrics fm = g2.getFontMetrics();

            String text1 = "GAME OVER!";
            String text2 = "Your score: " + kills;
            String text3 = "Highscore: " + highscore;
            String text4 = "Press [Space] or [A] to restart.";

            Rectangle2D r = fm.getStringBounds(text1, g2);
            g2.drawString(text1, centerX - (int) (r.getWidth() / 2), centerY - 90);
            r = fm.getStringBounds(text2, g2);
            g2.drawString(text2, centerX - (int) (r.getWidth() / 2), centerY - 60);
            r = fm.getStringBounds(text3, g2);
            g2.drawString(text3, centerX - (int) (r.getWidth() / 2), centerY - 30);
            r = fm.getStringBounds(text4, g2);
            g2.drawString(text4, centerX - (int) (r.getWidth() / 2), centerY + 30);
        } else {
            g2.drawString("SPACE SHOOTER - v0.9", 10, 30);
            g2.drawString("KILLS: " + kills, 10, 50);
            g2.drawString("COMBO: " + combo, 10, 70);
            g2.drawString("SHIELD: " + flashes, 10, 90);
        }

        if (paused) {
            String text = "Pause. Press [Esc] or [Start] to continue.";
            g2.setFont(boldFont);
            g2.setColor(Color.white);
            FontMetrics fm = g2.getFontMetrics();
            Rectangle2D r = fm.getStringBounds(text, g2);

            g2.drawString(text, centerX - (int) (r.getWidth() / 2), centerY - 40);
        }
    }

    void gameLoop() {
        long lastLoopTime = System.nanoTime();
        final int TARGET_FPS = 60;
        final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

        // keep looping round til the game ends
        while (gameRunning) {
            long now = System.nanoTime();
            long updateLength = now - lastLoopTime;
            double delta = updateLength / ((double) OPTIMAL_TIME);
            lastLoopTime = now;

            lastFpsTime += updateLength;
            fps++;

            if (lastFpsTime >= 1000000000) {
                currentFPS = fps;
                if (!gameOver && !paused) {
                    spawnEnemy();
                    spawnEnemy();
                    spawnEnemy();
                }
                lastFpsTime = 0;
                fps = 0;
            }

            if (!gameOver && !paused) {
                update(delta);
                render();
            }

            repaint();

            try {
                Thread.sleep((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000);
            } catch (Exception ignored) {
            }
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D componentGraphics = (Graphics2D) g;
        componentGraphics.drawImage(canvas, null, 0, 0);
    }
}