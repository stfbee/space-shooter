import java.awt.*;
import java.util.Random;

/**
 * User: Vlad
 * Date: 19.09.13
 * Time: 2:43
 */
class Bullet {
    private final double angleRad;
//    private int startX1, startY1, startX2, startY2, startX3, startY3, startX4, startY4;
//    public int x1, x2, x3, x4, y1, y2, y3, y4;
    public int x1, x2, y1, y2;
    public final Color color;
    private double velocity;
    private double position = 10;

    public Bullet(int angle) {

        //Рандоматизация цвета пули
        Random rand = new Random();
        int r = rand.nextInt(90);
        int g = 255;
        int b = rand.nextInt(155);
        if (r > b) {b = 0;} else {r = 0;}
        color = new Color(r, g, b);

        //Отклонение пули (в градусах)
//        int deltaAngle = 2;
        angleRad = (angle +Math.random() % 360) * Math.PI / 180;
//        angleRad = (angle +(Math.random() * (deltaAngle*2 + 1) - deltaAngle) % 360) * Math.PI / 180;
//        angleRad = (360 - (angle +(Math.random() * (deltaAngle*2 + 1)) - deltaAngle) % 360) * Math.PI / 180;

        //Начальные координаты пуль
//        startX1 = 0; startY1 = -5;
//        startX2 = 10; startY2 = -5;
//        startX3 = 0; startY3 = 5;
//        startX4 = 10; startY4 = 5;
    }

    public void move(double delta) {
        velocity += .125 * delta;
        position += velocity * delta;

        x1 = (int) (Math.cos(angleRad) * (position));
        y1 = (int) (Math.sin(angleRad) * (position));
        x2 = (int) (Math.cos(angleRad) * (position + 10));
        y2 = (int) (Math.sin(angleRad) * (position + 10));

//        Заготовка для поверапа
//        x1 = (int) ((startX1 + position) * Math.cos(angleRad) + startY1 * Math.sin(angleRad));
//        y1 = (int) (-(startX1 + position) * Math.sin(angleRad) + startY1 * Math.cos(angleRad));
//        x2 = (int) ((startX2 + position) * Math.cos(angleRad) + startY2 * Math.sin(angleRad));
//        y2 = (int) (-(startX2 + position) * Math.sin(angleRad) + startY2 * Math.cos(angleRad));
//
//        x3 = (int) ((startX3 + position) * Math.cos(angleRad) + startY3 * Math.sin(angleRad));
//        y3 = (int) (-(startX3 + position) * Math.sin(angleRad) + startY3 * Math.cos(angleRad));
//        x4 = (int) ((startX4 + position) * Math.cos(angleRad) + startY4 * Math.sin(angleRad));
//        y4 = (int) (-(startX4 + position) * Math.sin(angleRad) + startY4 * Math.cos(angleRad));



    }
}
