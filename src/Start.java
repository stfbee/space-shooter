import java.io.*;
import java.lang.reflect.Field;

/**
 * User: vladik
 * Date: 10.10.13
 * Time: 9:14
 */
public class Start {
    private static final String OS = System.getProperty("os.name").toLowerCase();
    private static final String ARCH = System.getProperty("os.arch").toLowerCase();
    static String path = System.getProperty("user.home") + File.separator + ".spaceshooter" + File.separator;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void main(String[] args) {
        File f = new File(path + "lib" + File.separator);

        f.mkdir();

        try {
            System.setProperty("java.library.path", path + "lib" + File.separator);
            System.out.println(System.getProperty("java.library.path"));
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (NoSuchFieldException ignored) {
        } catch (IllegalAccessException ignored) {
        }

        if (isWindows()) {
            if (!is64()) {
                extractFile("lib/jinput-dx8.dll");
                extractFile("lib/jinput-raw.dll");
            } else {
                extractFile("lib/jinput-dx8_64.dll");
                extractFile("lib/jinput-raw_64.dll");
            }
            extractFile("lib/jinput-wintab.dll");
        } else if (isMac()) {
            extractFile("lib/libjinput-osx.jnilib");
        } else if (isUnix()) {
            if (!is64()) {
                extractFile("lib/libjinput-linux.so");
            } else {
                extractFile("lib/libjinput-linux64.so");
            }
        } else {
            System.out.println("Your OS is not support!!");
            System.exit(0);
        }
        Game.main(args);
    }

    private static void extractFile(String name) {

        try {
            ClassLoader cl = Start.class.getClassLoader();
            File target = new File(path + name);
            if (target.exists())
                return;

            FileOutputStream out;
            out = new FileOutputStream(target);

            InputStream in = cl.getResourceAsStream(name);

            byte[] buf = new byte[8 * 1024];
            int len;
            while ((len = in.read(buf)) != -1) {
                out.write(buf, 0, len);
            }

            out.close();
            in.close();

        } catch (FileNotFoundException ignored) {
        } catch (IOException ignored) {
        }
    }

    private static boolean isWindows() {
        return (OS.contains("win"));
    }

    private static boolean isMac() {
        return (OS.contains("mac"));
    }

    private static boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0);
    }

    private static boolean is64() {
        return (ARCH.contains("64"));
    }

}